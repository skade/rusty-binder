//! Functions for actually parsing the source file.

use compiler;
use compiler::session::Session;
use compiler::Stop;
use compiler::utils;
use syntax::ast;

/// Type responsible for generating a given language's bindings.
///
/// This holds the given language's `Compiler` and `Generator`s for any languages that it relies
/// on.
pub struct Generator {
    compiler: Box<compiler::Compiler>,
    dependencies: Vec<Generator>,
}

impl Generator {
    /// Recursively create a compiler for a given language and any dependent compilers.
    pub fn new(compiler: Box<compiler::Compiler>, dependencies: Vec<Box<compiler::Compiler>>) -> Generator {
        let mut generator_dependencies = vec![];

        for dep in dependencies {
            let recursive_dependencies = dep.dependencies();
            generator_dependencies.push(Generator::new(dep, recursive_dependencies));
        }

        Generator {
            compiler: compiler,
            dependencies: generator_dependencies,
        }
    }
}


/// Check that an expected path has been `pub use`d.
fn check_pub_use(item: &ast::Item, expected: &ast::Path) -> bool {
    if let ast::ItemKind::Use(ref path) = item.node {
        // API has to be public to be used.
        if let ast::Visibility::Public = item.vis {
            // Easiest way to ensure all of API has been brought into scope.
            if let ast::ViewPath_::ViewPathGlob(ref path) = path.node {
                return path.segments == expected.segments;
            }
        }
    }

    false
}


/// Recursively call `.compile_crate()` on all compilers.
fn apply_compile_crate(krate: &ast::Crate, session: &mut Session, compilers: &mut [Generator]) -> Result<(), Stop> {
    for compiler in compilers {
        try!(compiler.compiler.compile_crate(session, krate));
        try!(apply_compile_crate(krate, session, &mut compiler.dependencies));
    }

    Ok(())
}

/// Recursively call `.compile_mod()` on all compilers.
fn apply_compile_mod(module: &ast::Mod, session: &mut Session, compilers: &mut [Generator]) -> Result<(), Stop> {
    for compiler in compilers {
        try!(compiler.compiler.compile_mod(session, module));
        try!(apply_compile_mod(module, session, &mut compiler.dependencies));
    }

    Ok(())
}

/// Recursively call `.compile_item()` on all compilers.
fn apply_compile_item(item: &ast::Item, session: &mut Session, compilers: &mut [Generator]) -> Result<(), Stop> {
    for compiler in compilers {
        try!(compiler.compiler.compile_item(session, item));
        try!(apply_compile_item(item, session, &mut compiler.dependencies));
    }

    Ok(())
}

/// Recursively call `.compile_ty_item()` on all compilers.
fn apply_compile_ty_item(item: &ast::Item, session: &mut Session, compilers: &mut [Generator]) -> Result<(), Stop> {
    for compiler in compilers {
        try!(compiler.compiler.compile_ty_item(session, item));
        try!(apply_compile_ty_item(item, session, &mut compiler.dependencies));
    }

    Ok(())
}

/// Recursively call `.compile_enum_item()` on all compilers.
fn apply_compile_enum_item(item: &ast::Item, session: &mut Session, compilers: &mut [Generator]) -> Result<(), Stop> {
    for compiler in compilers {
        try!(compiler.compiler.compile_enum_item(session, item));
        try!(apply_compile_enum_item(item, session, &mut compiler.dependencies));
    }

    Ok(())
}

/// Recursively call `.compile_struct_item()` on all compilers.
fn apply_compile_struct_item(item: &ast::Item, session: &mut Session, compilers: &mut [Generator]) -> Result<(), Stop> {
    for compiler in compilers {
        try!(compiler.compiler.compile_struct_item(session, item));
        try!(apply_compile_struct_item(item, session, &mut compiler.dependencies));
    }

    Ok(())
}

/// Recursively call `.compile_fn_item()` on all compilers.
fn apply_compile_fn_item(item: &ast::Item, session: &mut Session, compilers: &mut [Generator]) -> Result<(), Stop> {
    for compiler in compilers {
        try!(compiler.compiler.compile_fn_item(session, item));
        try!(apply_compile_fn_item(item, session, &mut compiler.dependencies));
    }

    Ok(())
}


/// The main entry point when looking for a specific module.
///
/// Determines which module to parse, ensures it is `pub use`ed then hands off to `parse_mod`.
pub fn parse_crate(krate: &ast::Crate, session: &mut Session, compilers: &mut [Generator]) -> Result<Vec<super::Binding>, ::std::io::Error> {
    try!(apply_compile_crate(krate, session, compilers)
        .map_err(|_| ::std::io::Error::new(::std::io::ErrorKind::Other, "failed to compile crate")));

    let mut current_module = &krate.module;

    if let Some(ref module) = session.module {
        // For each module in the path, look for the corresponding module in the source.
        for module in &module.segments {
            let mut found = false;
            for item in &current_module.items {
                if let ast::ItemKind::Mod(ref new_module) = item.node {
                    if module.identifier == item.ident {
                        current_module = new_module;
                        found = true;
                        break;
                    }
                }
            }

            if !found {
                session.fatal(&format!(
                    "module `{}` could not be found",
                    module.identifier
                ));

                return Err(::std::io::Error::new(::std::io::ErrorKind::Other, "compilation error"));
            }
        }

        // Look to see if the module has been `pub use`d.
        if !krate.module.items.iter().any(|item| check_pub_use(item, module)) {
            session.fatal_with_help(
                &format!("module `{}` has not been brought into global scope", module),
                &format!("try putting `pub use {}::*` in your root source file", module),
            );

            return Err(::std::io::Error::new(::std::io::ErrorKind::Other, "compilation error"));
        }
    }

    parse_mod(current_module, session, compilers)
}

/// The main parsing method.
///
/// Iterates through all items in the module and dispatches to the given compilers then pulls all
/// the compiled bindings together.
fn parse_mod(module: &ast::Mod, session: &mut Session, compilers: &mut [Generator]) -> Result<Vec<super::Binding>, ::std::io::Error> {
    try!(apply_compile_mod(module, session, compilers)
        .map_err(|_| ::std::io::Error::new(::std::io::ErrorKind::Other, "failed to compile module")));

    for item in &module.items {
        try!(apply_compile_item(item, session, compilers)
            .map_err(|_| ::std::io::Error::new(::std::io::ErrorKind::Other, "failed to compile item")));

        // Most bindings will require a C ABI, which in turn requires that the item is visible.
        if let ast::Visibility::Inherited = item.vis { continue; }

        // Dispatch to correct method.
        let res = match item.node {
            ast::ItemKind::Ty(..) => parse_ty(item, session, compilers),
            ast::ItemKind::Enum(..) => parse_enum(item, session, compilers),
            ast::ItemKind::Struct(..) => parse_struct(item, session, compilers),
            ast::ItemKind::Fn(..) => parse_fn(item, session, compilers),
            _ => Ok(()),
        };

        if let Err(Stop::Fail) = res {
            return Err(::std::io::Error::new(
                ::std::io::ErrorKind::Other,
                "parsing failure",
            ));
        }
    }

    Ok(compile_bindings(compilers))
}

fn compile_bindings(compilers: &mut [Generator]) -> Vec<super::Binding> {
    let mut bindings = vec![];

    for compiler in compilers {
        let dependent_bindings = compile_bindings(&mut compiler.dependencies);
        bindings.push(super::Binding {
            language: compiler.compiler.language(),
            files: compiler.compiler.compile_bindings(),
            dependencies: dependent_bindings,
        })
    }

    bindings
}

fn parse_ty(item: &ast::Item, session: &mut Session, compilers: &mut [Generator]) -> Result<(), Stop> {
    apply_compile_ty_item(item, session, compilers)
}

fn parse_enum(item: &ast::Item, session: &mut Session, compilers: &mut [Generator]) -> Result<(), Stop> {
    // If it's not #[repr(C)] then it can't be called from C.
    if !utils::has_c_repr(item, session) { return Ok(()); }

    if let ast::ItemKind::Enum(_, ref generics) = item.node {
        if generics.is_parameterized() {
            return Err(session.span_err(
                item.span,
                "binder can not handle parameterized `#[repr(C)]` enums",
            ));
        }
    } else {
        session.span_bug_with_note(
            item.span,
            "`parse_enum` called on wrong `Item_`",
            "expected `syntax::ast::Item_::ItemEnum`",
        );
    }

    apply_compile_enum_item(item, session, compilers)
}

fn parse_struct(item: &ast::Item, session: &mut Session, compilers: &mut [Generator]) -> Result<(), Stop> {
    // If it's not #[repr(C)] then it can't be called from C.
    if !utils::has_c_repr(item, session) { return Ok(()); }

    if let ast::ItemKind::Struct(ref variants, ref generics) = item.node {
        if generics.is_parameterized() {
            return Err(session.span_err(
                item.span,
                "binder can not handle parameterized `#[repr(C)]` structs",
            ));
        }

        if variants.is_unit() {
            return Err(session.span_err(
                item.span,
                "binder can not handle unit `#[repr(C)]` structs",
            ));
        }
    } else {
        session.span_bug_with_note(
            item.span,
            "`parse_struct` called on wrong `Item_`",
            "expected `syntax::ast::Item_::ItemStruct`",
        );
    }

    apply_compile_struct_item(item, session, compilers)
}

fn parse_fn(item: &ast::Item, session: &mut Session, compilers: &mut [Generator]) -> Result<(), Stop> {
    if !utils::is_no_mangle(item) { return Err(Stop::Abort) }

    if let ast::ItemKind::Fn(_, _, _, abi, ref generics, _) = item.node {
        use syntax::abi::Abi;
        match abi {
            // If it doesn't have a C ABI it can't be called from C.
            Abi::C | Abi::Cdecl | Abi::Stdcall | Abi::Fastcall | Abi::System => {},
            _ => return Ok(()),
        }

        if generics.is_parameterized() {
            return Err(session.span_err(
                item.span,
                "binder can not handle parameterized extern functions",
            ));
        };
    } else {
        session.span_bug_with_note(
            item.span,
            "`parse_fn` called on wrong `Item_`",
            "expected `syntax::ast::Item_::ItemFn`",
        );
    }

    apply_compile_fn_item(item, session, compilers)
}

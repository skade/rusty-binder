//! Relevant re-exports from `syntex_syntax`.
//!
//! If you think that there are other relevant types which should be exported here, please open an
//! issue on the [repo](https://gitlab.com/rusty-binder/rusty-binder).

pub mod abi {
    pub use syntax::abi::Abi;
}

pub mod ast {
    pub use syntax::ast::Attribute;
    pub use syntax::ast::BareFnTy;
    pub use syntax::ast::FnDecl;
    pub use syntax::ast::FunctionRetTy;
    pub use syntax::ast::Ident;
    pub use syntax::ast::Item;
    pub use syntax::ast::ItemKind;
    pub use syntax::ast::Mod;
    pub use syntax::ast::Mutability;
    pub use syntax::ast::MutTy;
    pub use syntax::ast::Path;
    pub use syntax::ast::PathSegment;
    pub use syntax::ast::PathListItemKind;
    pub use syntax::ast::Ty;
    pub use syntax::ast::TyKind;
    pub use syntax::ast::StructField;
    pub use syntax::ast::Crate;
    pub use syntax::ast::ViewPath_;
}

pub mod codemap {
    pub use syntax::codemap::BytePos;
    pub use syntax::codemap::Span;
    pub use syntax::codemap::spanned;
}

pub mod parse {
    pub use syntax::parse::new_parser_from_source_str;
    pub use syntax::parse::ParseSess;
    pub use syntax::parse::token;
}

pub mod print {
    pub use syntax::print::pprust;
}
